# 3DQue Firmware for 32bit Creality Printers

Upload this firmware to ensure that your Quinly upgraded printer functions as it should. This is a branch of Marlin and Jyers firmware which includes modifications that allow it to recieve and follow the commands of the Raspberry Pi properly. If you would like to compile your own firmware for your specific printer setup, you can base it off of this firmware, or follow the guide below on the nessesary changes for our system.

## How to Build Firmware from Existing Configs

Great news! We already configure firmware for the most popular Ender 3 setups, so you may not have to edit a single line of code to get your printer running. Either download one of the builds from the resources sections from our website or: 

* Find the appropriate Configuration.h and Configuration_adv.h from the "config" folder in this github page

* Copy it to the "Marlin" folder

* Compile the firmware and send it to your printer.

If you are unsure of how to compile firmware for your printer, there is [a great video by Teaching Tech](https://www.youtube.com/watch?v=eq_ygvHF29I) that shows you how to do it. Just make sure to use our Marlin and configurations instead of getting the files from Marlin's GitHub as he shows in the video.

## How to Modify Other Firmware to Work with Quinly

If you have a different mainboard, or just want to use the same firmware you were running before installing the Quinly upgrade, you will need to modify it slightly for it to work with our system. 

* First make sure it us based on Marlin firmware. This is easy to tell because it will have a folder called "Marlin" containing important files like ```Configuration.h``` and ```Configuration_adv.h```

### NOTE FOR ENDER 3 V2 USERS
If your firmware is older than Malrin 2.0.8.2 and not based on Jyers' firmware, Quinly will not be able to display the ip address needed to connect to it. In this case we highly recommend moving to our firmware, or the latest stable release of Marlin with our modifications as it will allow you to find Quinly's IP and connect to it.

After that, you'll need to make changes to the following files:

### Configuration.h

In Configuration.h, you'll need to modify/uncomment (remove the //) from the following lines:

* ```#define CUSTOM_MACHINE_NAME "Ender 3 V2"``` for Ender 3 V2/Ender 3 V2 clone users

OR ```#define CUSTOM_MACHINE_NAME "Ender 3"``` for other Ender 3 style printers

* ```#define RESTORE_LEVELING_AFTER_G28```

### Configuration_adv.h

* ```#define DEFAULT_STEPPER_DEACTIVE_TIME 0```

* ```#define EMERGENCY_PARSER```

* ```#define HOST_ACTION_COMMANDS```

With these simple changes you should be ready to go compile and send to your printer. If you are unsure of how to compile firmware for your printer, there is [a great video by Teaching Tech](https://www.youtube.com/watch?v=eq_ygvHF29I) that shows you how to do it. Just make sure to use our Marlin and configurations instead of getting the files from Marlin's GitHub as he shows in the video.

If you have any questions or run into any problems, feel free ask [our Discord](https://discord.com/invite/brtfDP5) or send your question to support@3dque.com.

Happy Printing!


## License

Marlin is published under the [GPL license](/LICENSE)
